<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/document">
    <html>
      <head>
        <link rel="stylesheet" type="text/css" href="main.css" />
        <title> <xsl:value-of select="@short-title" /> </title>

        <meta name="author" http-equiv="reply-to">
          <xsl:attribute name="content">
            <xsl:value-of select="@author" />
          </xsl:attribute>
        </meta>

        <meta name="description">
          <xsl:attribute name="content">
            <xsl:value-of select="@description" />
          </xsl:attribute>
        </meta>

        <meta name="keywords">
          <xsl:attribute name="content">
            <xsl:for-each select="keywords/keyword">
              <xsl:value-of select="."/>
              <xsl:if test="position() != last()">
                <xsl:text>, </xsl:text>
              </xsl:if>
            </xsl:for-each>
          </xsl:attribute>
        </meta>

      </head>
      <body>

        <header>
          <h1> <xsl:value-of select="@long-title" /> </h1>
        </header>

        <div class="menu-n-intro">

          <div class="intro">
            <xsl:copy-of select="introduction" />
          </div>

          <div class="menu">
            <nav>
              <ul>
                <xsl:for-each select="constructions/construction/heading">
                  <li>
                    <a>
                      <xsl:attribute name="href">
                        #<xsl:value-of select="@name" />
                      </xsl:attribute>
                      <xsl:value-of select="." />
                    </a>
                  </li>
                </xsl:for-each>
              </ul>
            </nav>
          </div>
          
        </div>

        <main>
          <xsl:for-each select="constructions/construction">
            <article>

              <h3>
                <a>
                  <xsl:attribute name="name">
                    <xsl:value-of select="heading/@name" />
                  </xsl:attribute>
                  <xsl:value-of select="./heading" />
                </a>
              </h3>

              <table>
                <tr>
                  <xsl:for-each select="/document/languages/language">
                    <th> <xsl:value-of select="."/> </th>
                  </xsl:for-each>
                </tr>
                <xsl:for-each select="samples/sample">
                  <xsl:variable name="sample" select="." />
                  <tr>
                    <xsl:for-each select="/document/languages/language">
                      <xsl:variable name="lang" select="@name" />
                      <td>
                        <xsl:for-each select="$sample/version[@lang=$lang]">
                          <xsl:copy-of select="*" />
                        </xsl:for-each>
                      </td>
                    </xsl:for-each>
                  </tr>
                </xsl:for-each>
              </table>

            </article>
          </xsl:for-each>
        </main>

        <footer>
          <div class="bottom-row">

            <div class="aux-info">
              <xsl:copy-of select="auxiliary" />
            </div>

            <div class="w3c-mark">
              <a href="http://validator.w3.org/check?uri=referer">
                <img src="http://www.w3.org/Icons/valid-xhtml10-blue"
                     alt="Valid XHTML 1.0 Transitional" height="31" width="88" />
              </a>
            </div>

          </div>
        </footer>

      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
